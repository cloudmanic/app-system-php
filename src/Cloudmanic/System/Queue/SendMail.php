<?php 
//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 03/23/2012
//

namespace Cloudmanic\System\Queue;

use \Mail;
use \View;
use Cloudmanic\Email\Message;
use Cloudmanic\System\Models\Accounts\Users;

class SendMail
{
	//
	// Route tasks that involve evernote.
	//
	public function fire($job, $data)
	{			
		$this->_log('Job: SendMail - ' . json_encode($data));
			
		// Setup the email we are about to send.	
		Message::load_laravel_4();
		Message::from($data['from']['email'], $data['from']['name']);
		Message::to($data['to']); 
		Message::subject($data['subject']);
		Message::message(View::make($data['view'], $data['data']));	
		
		// Send the email to the user.
		if(! Message::send())
		{
			$debug = Message::print_debugger([]);
			$this->_log('SendMail: Error sending Email To ' . $data['to']);  //. ' - ' . $debug);
		}
			
		// Delete Job.
		$this->_log('Status: SendMail - Sending Mail complete.');
		$job->delete(); 
	}
		
	//
	// Output a log message.
	//
	private function _log($msg)
	{
		echo '[' . date('n-j-Y g:i:s a') . "] $msg\n";
	}
}

/* End File */