<?php
//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 3/25/2013
//

namespace Cloudmanic\System\Models\Accounts;

class Users extends \Cloudmanic\System\Models\BasicModel
{	
	public static $connection = 'accounts';
	
	//
	// Delete a user.
	//
	public static function delete_by_id($id)
	{
		// Clean up scopes.
		OauthSess::set_col('OauthSessUserId', $id);
		if($sess = OauthSess::get())
		{
			OauthScopeLu::set_col('OauthScopeLuSessId', $sess[0]['OauthSessId']);
			OauthScopeLu::delete_all();
		}
		
		// Clean up some Sessions.
		OauthSess::set_col('OauthSessUserId', $id);
		OauthSess::delete_all();
		
		return parent::delete_by_id($id);
	}
	
	//
	// Get user by email.
	// 
	public static function get_by_email($email)
	{
		static::set_col('UsersEmail', trim($email));
		$u = static::get();
		return (isset($u[0])) ? $u[0] : false;
	}
}

/* End File */