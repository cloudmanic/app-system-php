<?php
//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 4/25/2013
//

namespace Cloudmanic\System\Models\Accounts;

class Plans extends \Cloudmanic\System\Models\BasicModel
{	
	public static $connection = 'accounts';
	
	//
	// Set active
	//
	public static function set_active()
	{
		static::set_col('PlansActive', 1);
	}
	
	//
	// Set code
	//
	public static function set_code($code)
	{
		static::set_col('PlansCode', strtoupper($code));
	}
	
	//
	// Get plan by code.
	//
	public static function get_by_code($code)
	{
		static::set_code($code);
		$data = static::get();
		return (isset($data[0])) ? $data[0] : 0; 
	}
}

/* End File */