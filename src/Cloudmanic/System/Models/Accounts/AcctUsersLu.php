<?php
//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 3/12/2013
//

namespace Cloudmanic\System\Models\Accounts;

class AcctUsersLu extends \Cloudmanic\System\Models\BasicModel
{	
	public static $connection = 'accounts';
	
	public static $remove = array('UsersPassword', 'UsersSalt');

	public static $joins = array(
		array('table' => 'Users', 'left' => 'AcctUsersLuUserId', 'right' => 'UsersId'),
		array('table' => 'Accounts', 'left' => 'AcctUsersLuAcctId', 'right' => 'AccountsId'),
		array('table' => 'Applications', 'left' => 'AccountsAppId', 'right' => 'ApplicationsId')
	);
	
	//
	// Get accounts by user.
	//
	public static function get_accounts_by_user($id)
	{
		static::set_col('AcctUsersLuUserId', $id);
		return static::get();
	}
	
	//
	// Get users by account.
	//
	public static function get_users_by_account($id)
	{
		static::set_col('AcctUsersLuAcctId', $id);
		return static::get();
	}
	
	//
	// Get a list of users for this account and index
	// it by id. This way we can do a fast look up
	// in a model since we can not join users anymore.
	//
	public static function get_index_user_list($acctid)
	{
		$rt = [];
		
		static::set_select([ 'UsersId', 'UsersFirstName', 'UsersLastName', 'UsersEmail', 'UsersAvatar' ]);
		foreach(static::get_users_by_account($acctid) AS $key => $row)
		{
			$rt[$row['UsersId']] = $row;
		}
		
		return $rt;
	}
}

/* End File */