<?php
//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 4/15/2013
//

namespace Cloudmanic\System\Models\Accounts;

class AcctHistory extends \Cloudmanic\System\Models\BasicModel
{	
	public static $connection = 'accounts';
}

/* End File */