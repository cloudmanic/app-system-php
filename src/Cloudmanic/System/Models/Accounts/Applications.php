<?php
//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 3/12/2013
//

namespace Cloudmanic\System\Models\Accounts;

class Applications extends \Cloudmanic\System\Models\BasicModel
{	
	public static $connection = 'accounts';
	
	//
	// Get application id by the application code.
	//
	public static function get_id_by_code($code)
	{
		$d = static::get_by_code($code);
		return (isset($d['ApplicationsId'])) ? $d['ApplicationsId'] : false;
	}
	
	//
	// Get application by the application code.
	//
	public static function get_by_code($code)
	{
		static::set_col('ApplicationsCode', strtoupper($code));
		$d = static::get();
		return (isset($d[0])) ? $d[0] : false;
	}
}

/* End File */