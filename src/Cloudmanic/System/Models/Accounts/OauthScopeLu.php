<?php
//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 4/16/2013
//

namespace Cloudmanic\System\Models\Accounts;

class OauthScopeLu extends \Cloudmanic\System\Models\BasicModel
{	
	public static $connection = 'accounts';
	public static $joins = [
		[ 'table' => 'OauthScopes', 'left' => 'OauthScopeLuScopeId', 'right' => 'OauthScopesId' ]
	];
	
	//
	// Get By Scopes by sess id.
	//
	public static function get_scopes_by_sess_id($id)
	{
		static::set_col('OauthScopeLuSessId', $id);
		return static::get();
	}
}

/* End File */