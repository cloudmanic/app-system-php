<?php
//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 3/25/2013
//

namespace Cloudmanic\System\Models\Accounts;

use \DB;

class Accounts extends \Cloudmanic\System\Models\BasicModel
{	
	public static $connection = 'accounts';
	
	public static $joins = array(
		array('table' => 'Applications', 'right' => 'ApplicationsId', 'left' => 'AccountsAppId'),
		array('table' => 'Users', 'right' => 'UsersId', 'left' => 'AccountsOwnerId'),
		array('table' => 'Plans', 'right' => 'PlansId', 'left' => 'AccountsPlanId')
	);
	
	//
	// Set search.
	//
	public static function set_search($str)
	{
		static::get_query()->where(function($query) use ($str)
		{
			$query->orWhere('AccountsId', 'like', '%' . $str . '%')
				->orWhere('AccountsDisplayName', 'like', '%' . $str . '%')
				->orWhere('UsersFirstName', 'like', '%' . $str . '%')
				->orWhere('UsersLastName', 'like', '%' . $str . '%')
				->orWhere('UsersEmail', 'like', '%' . $str . '%')
				->orWhere('PlansName', 'like', '%' . $str . '%');
    });
	}
	
	//
	// Get by stripe id.
	//
	public static function get_by_stripe($id)
	{
		static::set_col('AccountsStripeId', $id);
		$acct = static::get();
		return (isset($acct[0])) ? $acct[0] : false;		
	}
	
	//
	// Get a summary of the accounts by month.
	//
	public static function total_per_month()
	{
		$data = [];
		
		$sql = 'SELECT *, (@runtot := @runtot + a.Count) AS Total FROM
						(SELECT COUNT(AccountsId) AS Count, EXTRACT(MONTH FROM AccountsCreatedAt) AS Month, 
						EXTRACT(YEAR FROM AccountsCreatedAt) AS Year, EXTRACT(YEAR_MONTH FROM AccountsCreatedAt) 
						AS Cohort FROM Accounts GROUP BY Cohort) AS a';
						
		$pdo = DB::connection(static::$connection)->getPdo();
		$pdo->query('SET @runtot:=0');
		
		foreach($pdo->query($sql) AS $key => $row) 
		{
			$data[] = [
				'Month' => $row['Month'], 
				'Year' => $row['Year'],
				'Cohort' => $row['Cohort'],
				'Count' => $row['Count'],
				'Total' => $row['Total']
			];
		}
		
		return $data;
	}
	
	//
	// Get. _format_get does not work with the basic model.
	//
	public static function get()
	{
		$data = parent::get();
		
		foreach($data AS $key => $row)
		{
			static::_format_get($data[$key]);
		}
		
		return $data;
	}
	
	//
	// Format Get.
	//
	public static function _format_get(&$data)
	{
		// Date formatting - AccountsCreatedAt.
		if(isset($data['AccountsCreatedAt']))
		{
			$data['AccountsCreatedAt_df1'] = date('n/j/Y', strtotime($data['AccountsCreatedAt']));
		}
		
		// Date formatting - AccountsLastActivity.
		if(isset($data['AccountsLastActivity']))
		{
			if($data['AccountsLastActivity'] != '0000-00-00 00:00:00')
			{
				$data['AccountsLastActivity_df1'] = date('n/j/Y', strtotime($data['AccountsLastActivity']));
			} else
			{
				$data['AccountsLastActivity_df1'] = '----';
			}
		}
	}
}

/* End File */