<?php
//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 3/25/2013
//

namespace Cloudmanic\System\Models;

class DupKeys extends \Cloudmanic\System\Models\AcctModel
{	
	//
	// Get by duplicate key.
	//
	public static function get_by_key($key)
	{
		static::set_col('DupKeysHash', $key);
		$d = static::get();
		return (isset($d[0])) ? $d[0]['DupKeysId'] : 0;
	}
}

/* End File */