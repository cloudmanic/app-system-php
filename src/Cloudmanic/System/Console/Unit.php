<?php 
//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 04/25/2013
//

namespace Cloudmanic\System\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Unit extends Command 
{
	//
	// Configure.
	//
	protected function configure()
	{
		$this->setName('unit:run');
		$this->setDescription('Run all of the Cloudmanic Labs unit tests.');
	}

	//
	// Run command.
	//
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln("\n");
		$output->writeln('<info>##### Starting Tests #####</info>');
		$output->writeln("\n");

		// Run the tests.
		\Cloudmanic\System\Libraries\Unit::run_tests($output);
		
		// Display the summary.
		$output->writeln("\n");
		$summary = \Cloudmanic\System\Libraries\Unit::$summary;
		$output->writeln('Total: ' . $summary['total']);
		$output->writeln('Error: ' . $summary['error']);
		
		$output->writeln("\n");
		$output->writeln('<info>##### Test Complete #####</info>');
		$output->writeln("\n");
	}
}

/* End File */