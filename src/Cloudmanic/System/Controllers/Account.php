<?php

namespace Cloudmanic\System\Controllers;

use Cloudmanic\System\Libraries\Me;
use Cloudmanic\System\Models\Accounts\Accounts;
use Cloudmanic\System\Models\Accounts\AcctUsersLu;
use \Input;
use \Validator;

class Account extends ApiController 
{
	public $rules_create = [];
	public $rules_message = [];
	
	//
	// Get the account.
	//
	public function index()
	{
		Accounts::set_select([ 'AccountsId', 'AccountsDisplayName' ]);
		$account = Accounts::get_by_id(Me::get_account_id());		
		return $this->api_response($account, 1, NULL, NULL, false);
	}
	
	//
	// Get my accounts by app.
	//
	public function accounts_by_app()
	{
		$curr = Me::get_account();
	
		AcctUsersLu::set_order('AccountsDisplayName', 'asc');
		AcctUsersLu::set_select([ 'AccountsId', 'AccountsAppId', 'AccountsDisplayName' ]);
		AcctUsersLu::set_col('AccountsAppId', $curr['AccountsAppId']);
		$accts = AcctUsersLu::get_accounts_by_user(Me::get('UsersId'));
		
		return $this->api_response($accts, 1, NULL, NULL, false);
	}
	
	//
	// Update the display name.
	//
	public function update_display_name()
	{
		$validator = Validator::make(Input::all(), [ 'AccountsDisplayName' => 'required|min:1' ]);
		
		// Validate the file upload.
		if($validator->fails())
		{
			$messages = $validator->messages();			
			return $this->api_response(null, 0, $messages);
		}
		
		// Update the display name.
		Accounts::update([ 'AccountsDisplayName' => Input::get('AccountsDisplayName') ], Me::get_account_id());
		return $this->api_response();
	}
}