<?php

//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 7/22/2014
//

namespace Cloudmanic\System\Controllers;

class DeleteLog extends Api
{
	public $model_name = 'Cloudmanic\System\Models\DeleteLog';
}

/* End File */