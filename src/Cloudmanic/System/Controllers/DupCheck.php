<?php

//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 8/12/2013

namespace Cloudmanic\System\Controllers;

use \Input;
use Cloudmanic\System\Libraries\Me;
use Cloudmanic\System\Libraries\Events;
use Cloudmanic\System\Models\DupKeys;

class DupCheck extends ApiController 
{	
	//
	// Post.
	//
	public function post()
	{
		$data = [];
	 	
	 	// Make sure we posted a DupKey
		if(! Input::get('DupKey'))
		{
			return $this->api_response([], 0, null, [ [ 'error' => 'Most post a DupKey.', 'field' => 'DupKey' ] ]);
		}
	
		// Check the DupKey
		$data['id'] = DupKeys::get_by_key(Input::get('DupKey'));
		
		return $this->api_response($data, 1, null, null, false);
	}
}

/* End File */