<?php
//
// Company: Cloudmanic Labs, LLC
// Website: http://cloudmanic.com
// Date: 3/15/2013
//

namespace Cloudmanic\System\Libraries;

use Cloudmanic\Api\Evermanic as Evermanic;
use Requests as Requests;

class Unit 
{
	public static $output = null;
	public static $summary = array('total' => 0, 'error' => 0, 'success' => 0);
	protected $_methods = array();
	protected $_called_class = '';
	private static $_configs = array();
	private static $_paths = array('../tests/system-pre/*', '../tests/api/*', '../tests/system-post/*');
	
	//
	// Construct.
	//
	function __construct()
	{
		$this->_called_class = get_called_class();
		$this->_methods = get_class_methods($this->_called_class);
		$this->_run_tests();
	}
	
	//
	// Loop through all the methods in the test and run them.
	//
	private function _run_tests()
	{
		foreach($this->_methods AS $key => $row)
		{
			if(strpos($row, 'test_') === 0)
			{			
				$this->{$row}();
			}
		}
	}
	
	// ---------------- Static Functions ------------------ //
	
	//
	// Run the test.
	//
	public static function run_tests($output)
	{
		// Setup the output variable.
		static::$output = $output;
	
		// Run through the different unit testing paths.
		foreach(self::$_paths AS $key => $row)
		{
			$files = glob($row);
			foreach($files AS $key2 => $row2)
			{
				include($row2);
			}
		}			
	}
	
	//
	// Test to see if two vars are equal.
	//
	public static function assert_equal($var1, $var2, $msg)
	{
		if($var1 == $var2)
		{
			Unit::log_status('ok', $msg);
		} else
		{
			Unit::log_status('fail', $msg);
		}
	}	
	

	//
	// Test to see if a number is greater than zero.
	//
	public static function assert_greater_than_zero($num, $msg)
	{		
		if($num > 0)
		{
			Unit::log_status('ok', $msg);
		} else
		{
			Unit::log_status('fail', $msg);
		}
	}
	
	//
	// Test the number of fields returned in the user profile.
	//
	public static function assert_array_count($data, $count, $msg)
	{		
		if(count($data) == $count)
		{
			Unit::log_status('ok', $msg);
		} else
		{
			Unit::log_status('fail', $msg);
		}
	}
	
	//
	// Test to see if an array index exists.
	//
	public static function assert_array_key($data, $key, $msg)
	{
		if(array_key_exists($key, $data))
		{
			Unit::log_status('ok', $msg);
		} else
		{
			Unit::log_status('fail', $msg);
		}
	}	
	
	//
	// Set a config value.
	//
	public static function set_config($key, $value)
	{
		self::$_configs[$key] = $value;
	}
	
	//
	// Get a config value.
	//
	public static function get_config($key)
	{
		return (isset(self::$_configs[$key])) ? self::$_configs[$key] : '';
	}
	
	//
	// Log success / failure.
	//
	public static function log_status($status, $msg)
	{
		static::$summary['total'] += 1; 
		
		if($status == 'ok')
		{
			static::$summary['success'] += 1; 
			static::$output->writeln("[$status]: $msg");
		} else
		{
			static::$summary['error'] += 1; 
			static::$output->writeln("<error>[$status]: $msg</error>");			
		}
	}
}

/* End File */