<?php 
//
// Company: Cloudmanic Labs, LLC
// Website: http://cloudmanic.com
// Date: 4/4/2013
//

namespace Cloudmanic\System\Libraries;

use Illuminate\Support\Facades\DB as DB;
use Illuminate\Support\Facades\Config as Config;
use Illuminate\Support\Facades\Session as Session;
use Illuminate\Support\Facades\Input as Input;
use Cloudmanic\System\Models\Accounts\OauthSess as OauthSess;
use Cloudmanic\System\Models\Accounts\AcctUsersLu as AcctUsersLu;
use Cloudmanic\System\Models\Accounts\Users as Users;
use Cloudmanic\System\Models\Accounts\Accounts as Accounts;
use Cloudmanic\System\Libraries\Me as Me;
use Cloudmanic\System\Libraries\App as App;

class CloudAuth
{
	public static $account = null;
	public static $error = '';
	
	//
	// Authenicate an API call. Set the "Me" object if 
	// this authentication was a success.
	//
	public static function apiinit()
	{		
		// Setup vars.
		$access_token = null;
		$account_id = null;
			
		// First we see if we have passed in access token
		// and account_id. If not we check for a session.
		if(Input::get('access_token') && Input::get('account_id'))
		{
			$access_token = Input::get('access_token');
			$account_id = Input::get('account_id');
			App::set('connection', 'access_token');
		} else if(Session::has('access_token') && Session::has('account_id'))
		{
			$access_token = Session::get('access_token');
			$account_id = Session::get('account_id');
			App::set('connection', 'web');			
		} else if(isset($_COOKIE[Config::get('session.cookie') . '-ci']))
		{		
			// Check to see if we have a session.
			if($ci_sess = static::_get_ci_session())
			{
				$access_token = $ci_sess['access_token'];
				$account_id = $ci_sess['account_id'];
				App::set('connection', 'web');
			} else
			{
				static::$error = 'failed access_token (api): no session found.';
				return false;				
			}	
		} else 
		{
			static::$error = 'failed access_token (api): no session found.';
			return false;			
		}

		// Validate and get the user based on the access_token / account_id
		if(! static::validate_access_token($access_token, $account_id))
		{
			static::$error = 'validate_access_token (api): failed to validate.';
			return false;
		}
		
		// Set last activity
		static::set_last_activity();

		// If we made it this far we know we are good.
		return true;
	} 
		
	//
	// Authenticate this session. Set the "Me" object if 
	// this authentication was a success.
	//
	public static function sessioninit()
	{	
		$access_token = Session::get('access_token');
		$account_id = Session::get('account_id');
		App::set('connection', 'web');
				
		// First see if we have an access token.
		if($account_id && $access_token) 
		{
			// Validate and get the user based on the access_token / account_id
			if(! static::validate_access_token($access_token, $account_id))
			{
				static::$error = 'validate_access_token (sessioninit): failed to validate.';
				return false;
			} 
	
			// Set last activity
			static::set_last_activity();
			
			// If we made it this far we know we are good.
			return true;
		} else if(isset($_COOKIE[Config::get('session.cookie') . '-ci']))
		{				
			// Check to see if we have a session.
			if($ci_sess = static::_get_ci_session())
			{
				$access_token = $ci_sess['access_token'];
				$account_id = $ci_sess['account_id'];
				
				// Set Session data.
				Session::put('access_token', $access_token);
				Session::put('account_id', $account_id);				
				
				// Set last activity
				static::set_last_activity();				
				
				// If we made it this far we know we are good.
				return true;				
			} else
			{
				static::$error = 'failed access_token (sessioninit): no session found.';
				return false;				
			}	
		} else 
		{
			// Web requests we redirect the user to get an access_token.
			return static::get_access_token_web();
		}
	} 
	
	//
	// Set last activity.
	//
	public static function set_last_activity()
	{
		// Update the user last activty.	
		$q = [
			'UsersLastApp' => Config::get('site.app_code'),
			'UsersLastActivity' => date('Y-m-d G:i:s')
		];
		Users::update($q, Me::get('UsersId'));
		
		// Here we log the application usage. Just update the timestamp
		// in the applications database of the last time this app was accessed.
		$r = [ 'AccountsLastActivity' => $q['UsersLastActivity'] ];
		Accounts::update($r, Me::get_account_id());
	}
	
	//
	// Validate the access token and set the "Me" object.
	//
	public static function validate_access_token($access_token, $account_id)
	{
		// Make sure the access token is still valid.
		OauthSess::set_col('OauthSessStage', 'Granted');
		OauthSess::set_col('OauthSessToken', $access_token);
		if(! $sess = OauthSess::get())
		{
			Session::flush();
			return false;
		}
		
		// Make sure this user still has access to the account being requested.
		AcctUsersLu::set_col('AcctUsersLuUserId', $sess[0]['OauthSessUserId']);
		AcctUsersLu::set_col('AcctUsersLuAcctId', $account_id);
		if(! $lu = AcctUsersLu::get())
		{
			Session::flush();
			return false;
		}
		
		// Make sure this is still a valid user.
		if(! $user = Users::get_by_id($sess[0]['OauthSessUserId']))
		{
			Session::flush();
			return false;
		}
		
		// Make sure the account is still active.
		if(! $account = Accounts::get_by_id($account_id))
		{
			Session::flush();
			return false;
		}
		
		// Set the "Me" Object.
		Me::set($user);
		Me::set('access_token', $access_token);
		Me::set_account($account);
		
		// If we made it this far we are good.
		return true;
	}
	
	//
	// Get an access token by redirecting to accounts.cloudmanic.com
	//
	public static function get_access_token_web()
	{
		$auth = new \Cloudmanic\Api\Oauth([
			'client_id' => Config::get('site.app_clientid'),
			'client_secret' => Config::get('site.app_secret'),
			'client_redirect' => Config::get('site.app_redirect'),
			'client_scopes' => Config::get('site.app_scopes'),
			'client_env' => \Illuminate\Support\Facades\App::environment()
		]);
	
		// If we have an access token now we set it in the session.
		if($access_token = $auth->get_access_token())
		{
			// Get the user.
			if(! $user = $auth->get_user())
			{
				Session::flush();
				static::$error = 'get_access_token_web (get_user): No user.';
				return false;
			}
			
			// Make sure we have at least one account.
			if((! isset($user['Accounts'])) || (! isset($user['Accounts'][0])))
			{
				Session::flush();
				static::$error = 'Must have one account.';
				return false;
			}
			
			// Set Session data.
			Session::put('access_token', $access_token);
			Session::put('account_id', $user['Accounts'][0]['AccountsId']);
			
			// Session set. Redirect to get rid of the code in the query string.
			return 'redirect';
		}
	
		// Return failure.
		static::$error = 'get_access_token_web: made it to the end.';
		return false;
	}
	
	// -------------------- Private Helper Functions ------------- //
	
	//
	// Check to see if we have a session on a Codeigniter app. 
	//
	private static function _get_ci_session()
	{
		if(isset($_COOKIE[Config::get('session.cookie') . '-ci']))
		{
			$ci = unserialize($_COOKIE[Config::get('session.cookie') . '-ci']);
	
			if(isset($ci['session_id']))
			{
				$ci_sess = $ci['session_id'];
				
				// Query the database and get the session data.
				if($sess = DB::table('CiSessions')->where('session_id', '=', $ci_sess)->first())
				{
					$user_data = unserialize($sess->user_data);
					
					// Look in a few places for the access token.
					if(isset($user_data['AccessToken']))
					{
						$access_token = $user_data['AccessToken'];
					} 
					
					if(isset($user_data['LoggedIn']) && isset($user_data['LoggedIn']))
					{
						$access_token = $user_data['LoggedIn']['AccessToken'];					
					}
					
					// Grab the account id.
					if(isset($user_data['AccountId']))
					{
						$account_id = $user_data['AccountId'];
					}
					
					// Return the access token and account id.
					if(isset($account_id) && isset($access_token))
					{
						return [
							'account_id' => $account_id,
							'access_token' => $access_token
						];
					}
				}
			}
		}
		
		// If we made it this far we have no session.
		return false;
	}
}

/* End File */