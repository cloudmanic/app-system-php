<?php 
//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 10/7/2012
//

namespace Cloudmanic\Models;

class AcctUsersLu extends \Cloudmanic\Libraries\AppModel
{ 	
	protected static $connection = 'accounts';
	
	//
	// Set account.
	//
	public static function set_account($id)
	{
		self::set_col('AcctUsersLuAcctId', $id);
	}

	//
	// Join accounts.
	//
	public static function join_accounts()
	{
		self::get_query()->join('Accounts', 'AcctUsersLuAcctId', '=', 'AccountsId');
		self::get_query()->join('Applications', 'AccountsAppId', '=', 'ApplicationsId');
	}
}

/* End File */