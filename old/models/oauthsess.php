<?php 
//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 10/7/2012
//

namespace Cloudmanic\Models;

class OauthSess extends \Cloudmanic\Libraries\AppModel
{ 	
	protected static $connection = 'accounts';
}

/* End File */