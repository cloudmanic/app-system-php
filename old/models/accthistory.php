<?php 
//
// Company: Cloudmanic Labs, LLC
// Website: http://cloudmanic.com
// Date: 10/7/2012
//

namespace Cloudmanic\Models;

class AcctHistory extends \Cloudmanic\Libraries\AppModel
{ 	
	protected static $connection = 'accounts';
	
	//
	// Set account.
	//
	public static function set_account($id)
	{
		self::set_col('AcctHistoryAccountId', $id);
	}
}

/* End File */