<?php
//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 10/7/2012
//

namespace Cloudmanic\Controllers;

use Cloudmanic\Libraries\CloudAuth as CloudAuth;
use Laravel\Input as Input;
use Laravel\Response as Response;
use Laravel\Redirect as Redirect;
use Laravel\Routing\Router as Route;
use Laravel\Url as Url;


class ApiController extends \Laravel\Routing\Controller
{
	public $restful = true;
	public $model = '';
	 
	//
	// Construct.
	//
	public function __construct()
	{
		parent::__construct();
		$this->filter('before', 'api_auth');

		// Guess the model.
		$tmp = explode('_', get_called_class()); 
		$this->model = $tmp[2];
	}

	//
	// Insert.
	//
	public function post_insert()
	{		
		$m = $this->model;
		$data['Id'] = $m::insert(Input::get());	
		return $this->_response($data);
	}
	 
	//
	// Delete.
	//
	public function post_delete()
	{		
		$m = $this->model;
		$m::delete(Input::get('Id'));	
		return $this->_response(array());
	}
	 
	//
	// Get.
	//
	public function get_index()
	{		
		$m = $this->model;
		$data = $m::get();	
		return $this->_response($data);
	}
	
	//
	// Get by id. 
	// Returns status 0 if there was no data found.
	//
	public function get_id($id)
	{
		$m = $this->model;
		if($data = $m::get_by_id($id))
		{	
			return $this->_response($data);
		} else
		{
			return $this->_response(array(), 0, array('Entry not found.'));
		}
	}
	
	//
	// Return a response based on the get "format" param.
	//
	protected function _response($data, $status = 1, $errors = NULL)
	{
		$rt['status'] = $status;
		$rt['data'] = $data;
		
		// Set errors.
		if(is_null($errors))
		{
			$rt['errors'] = array();
		} else
		{
			$rt['errors'] = $errors;
		}
		
		switch(Input::get('format'))
		{
			case 'php':
				return '<pre>' . print_r($rt, TRUE) . '</pre>';
			break;
			
			default:
				return Response::json($rt);
			break;
		}
	}
}

/* End File */