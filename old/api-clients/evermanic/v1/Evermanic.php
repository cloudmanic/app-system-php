<?php
//
// By: Cloudmanic Labs, LLC 
// Web: http://cloudmanic.com/evermanic
// Date: 10/15/2012
//

class Evermanic
{
	private static $_apihost = 'http://evermanic.cloudmanic.dev/api/v1';
	private static $_request_url = '';
	private static $_response = '';
	private static $_raw_response = '';
	private static $_request_data = array();
	private static $_access_token = '';
	private static $_account_id = '';
	private static $_error = array();

	//
	// Set the access token.
	//
	public static function set_access_token($token)
	{
		self::$_access_token = trim($token);
	}
	
	//
	// Set the account id.
	//
	public static function set_account_id($id)
	{
		self::$_account_id = trim($id);
	}

	//
	// Set request data.
	//
	public static function set_data($key, $value)
	{
		self::$_request_data[$key] = $value;
	}

	//
	// Return error.
	//
	public static function get_error()
	{
		return self::$_error;
	}

	// ----------------------- Profiles API Requests ----------------------- //

	//
	// Insert profile entry.
	//
	public static function profile_create()
	{
		self::$_request_url = self::$_apihost . '/profiles/insert';
		return self::_request('post');
	} 
	
	//
	// Get profile entries.
	//
	public static function profiles_get()
	{
		self::$_request_url = self::$_apihost . '/profiles';
		return self::_request('get');
	}

	//
	// Get profile by id entry.
	//
	public static function profile_get_by_id($id)
	{
		self::$_request_url = self::$_apihost . '/profiles/id/' . $id;
		return self::_request('get');
	}  

	//
	// Delete ledger entry.
	//
	public static function profile_delete($id)
	{
		self::set_data('Id', $id);
		self::$_request_url = self::$_apihost . '/profiles/delete';
		return self::_request('post');	
	}

	// ----------------- Private Functions -------------------- //

	//
	// Make request to server
	//
	private static function _request($type)
	{
		// Reset error.
		self::$_error = array();

		// Set post / get requests we have to send with every request.
		self::$_request_data['access_token'] = self::$_access_token;
		self::$_request_data['account_id'] = self::$_account_id;
		self::$_request_data['format'] = 'json';

		// Is this a get request? If so tack on the params.
		if($type == 'get')
		{
			self::$_request_url = self::$_request_url . '?' . http_build_query(self::$_request_data);
		}

		// Setup request.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::$_request_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		
		// Is this a post requests?
		if($type == 'post')
		{
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, self::$_request_data);
		}
		
		// Send and decode the request.
		self::$_raw_response = curl_exec($ch);
		self::$_response = json_decode(self::$_raw_response, TRUE);
		self::$_request_data = array();
		self::$_request_url = '';
		curl_close($ch);
		
		//echo self::$_raw_response;
		
		// Make sure status was returned
		if(! isset(self::$_response['status']))
		{
			self::$_error[] = array('error' => 'Request failed', 'field' => 'N/A');
			return 0;
		}
		
		// Check for any errors.
		if(self::$_response['status'] == 0)
		{
			self::$_error = self::$_response['errors'];
			return false;
		}

		return self::$_response;
	}
}

/* End File */