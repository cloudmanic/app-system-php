<?php 
//
// Company: Cloudmanic Labs, LLC
// Website: http://cloudmanic.com
// Date: 10/7/2012
//

namespace Cloudmanic\Libraries;

use Laravel\Request as Request;

class Centcom 
{
	public static $account = array();
	
	//
	// Log event. We send a UDP request to a node.js
	// app that is listening. The node app does the rest.
	//
	public static function log_event($event)
	{
		// Make sure we really have an event.
		if(empty($event))
		{
			return FALSE;
		}
	
		// Setup the message.
		$acct = (isset(CloudAuth::$account['AccountsId'])) ? CloudAuth::$account['AccountsId'] : 0;
		$app = (isset(CloudAuth::$account['ApplicationsId'])) ? CloudAuth::$account['ApplicationsId'] : 0;
		$ip = Request::ip();
		$msg = trim($event) . ',' . $acct . ',' . $app . ',' . $ip . ','. date('Y-m-d G:i:s');
		$fp = pfsockopen('udp://127.0.0.1', 41234, $errno, $errstr);
		
		// Make sure we ahve a socket.
		if(! $fp)
		{
			return FALSE;
		}
	
		// Send the UDP Message.
		socket_set_timeout($fp, 10);
		fwrite($fp, $msg);
		fclose($fp);
		
		return TRUE;
	}
}

/* End File */