<?php
//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 10/7/2012
//

namespace Cloudmanic\Libraries;

use Laravel\Database as DB;
use Laravel\Config as Config;

class AppModel
{
	private static $no_account = false;
	protected static $table = '';
	protected static $query = null;
	protected static $connection = 'application';
	
	// ------------------------ Setters ------------------------------ //
	
	//
	// Set no account.
	//
	public static function set_no_account()
	{
		self::$no_account = true;	
	}
	
	//
	// Set order
	//
	public static function set_order($order, $sort = 'desc')
	{
		self::get_query()->order_by($order, $sort);
	}	
	
	//
	// Set Column.
	//
	public static function set_col($key, $value)
	{
		self::get_query()->where($key, '=', $value);
	}
	
	//
	// Set Columns to select.
	//
	public static function set_select($selects)
	{
		self::get_query()->select($selects);
	}
	
	// ------------------------ CRUD Functions ----------------------- //
	
	//
	// Get.
	// 
	public static function get()
	{	
 		// We want to return the object as an array.
		$current = Config::get('database.fetch');
		Config::set('database.fetch', \PDO::FETCH_ASSOC);
		
		// Make sure we have a query started.
		self::get_query();
		
		// Get the data and clear / reset configs. Add
		// Account where clause if needed.
		if(! self::$no_account) 
		{
			$data = self::get_query()->where(self::$table . 'AccountId', '=', CloudAuth::$account['AccountsId'])->get();
 		} else
 		{
	 		$data = self::get_query()->get();
 		}
		
		self::clear_query();
		Config::set('database.fetch', $current);
		
		return $data;
	} 
	
	//
	// Get by id.
	// 
	public static function get_by_id($id)
	{
		self::get_query();
		self::set_col(self::$table . 'Id', $id);
		$data = self::get();
		$rt = (isset($data[0])) ? (array) $data[0] : false;
		self::clear_query();
		return $rt;
	} 
	
	//
	// Insert.
	//
	public static function insert($data)
	{
		// Make sure we have a query started.
		self::get_query();
	
		// Add created at date
 		if(! isset($data[self::$table . 'CreatedAt'])) 
 		{
 			$data[self::$table  . 'CreatedAt'] = date('Y-m-d G:i:s');
 		}
	
 		// Add AccountId
 		if(! self::$no_account) 
 		{
	 		$data[self::$table  . 'AccountId'] = CloudAuth::$account['AccountsId'];
 		}
	
 		// Insert the data / clear the query and return the ID.
 		$id = self::get_query()->insert_get_id(self::_set_data($data));
 		self::clear_query();
 		return $id;
	}
	
	//
	// Update.
	//
	public static function update($data, $id)
	{	
		$rt = self::get_query()->where(self::$table . 'Id', '=', $id)->update(self::_set_data($data));
		self::clear_query();
		return $rt;
	}	

	//
	// Delete by id.
	//
	public static function delete($id)
	{
		// Make sure we have a query started.
		self::get_query();
		
		// Do we need to set the account.
		if(! self::$no_account) 
		{
			self::get_query()->where(self::$table . 'AccountId', '=', CloudAuth::$account['AccountsId']);
 		}
		
 		// Delete entry and clear query.
		self::get_query()->where(self::$table . 'Id', '=', $id)->delete();
		self::clear_query();
	}
	
	//
	// Delete all data.
	//
	public static function delete_all()
	{
		self::get_query()->delete();
		self::clear_query();
	}
	
	//
	// Delete all the data. (we are filtering by account typically).
	//
	public static function delete_account()
	{
		// Make sure we have a query started.
		self::get_query();
	
		// Do we need to set the account.
		if(! self::$no_account) 
		{
			self::get_query()->where(self::$table . 'AccountId', '=', CloudAuth::$account['AccountsId'])->delete();
 		} else
 		{
	 		self::get_query()->delete();
 		}	
 		
 		// Clear query.
 		self::clear_query();
	}
	
	// ----------------- Helper Function  -------------- //
	
	//
	// Get last query.
	//
	public static function get_last_query()
	{
		return end(DB::profile());
	}
	
	//
	// Clear a query. Typically run this after the 
	// database action is complete.
	//
	protected static function clear_query()
	{
		self::$query = null;
		self::$no_account = false;
	}
	
	//
	// If we have a query already under way return it. If not 
	// build the query and return a new object.
	//
	protected static function get_query()
	{
		if(is_null(self::$query))
		{
			$table = explode('\\', get_called_class());
			self::$table = end($table);
			self::$query = DB::table(self::$table, static::$connection);
			return self::$query;
		} else
		{
			return self::$query;
		}
	}
	
 	//
 	// This will take the post data and filter out the non table cols.
 	//
	private static function _set_data($data)
 	{
 		$q = array();
 		$current = Config::get('database.default');
 		Config::set('database.default', static::$connection); 
 		$fields = DB::query('SHOW COLUMNS FROM ' . self::$table);
 		Config::set('database.default', $current);
 		
 		foreach($fields AS $key => $row)
 		{ 
 			if(isset($data[$row->Field])) 
 			{
 				$q[$row->Field] = $data[$row->Field];
 			}
 		}
 		
 		return $q;
 	}
}

/* End File */