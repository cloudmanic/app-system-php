<?php 
//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 10/7/2012
//

namespace Cloudmanic\Libraries;

class Me
{
	private static $data = array();

	//
	// Get one index in the data array.
	//
	public static function val($key)
	{
		return (isset(self::$data[$key])) ? self::$data[$key] : '';
	}

	//
	// Get logged in user.
	//
	public static function get()
	{
		return self::$data;
	}

	//
	// Set logged in user.
	//
	public static function set($data)
	{
		self::$data = $data;
	}
}

/* End File */