<?php 
//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 10/13/2012
//

namespace Cloudmanic\libraries;

class Start
{
	//
	// Laravel Init. Stuff to setup that is useful 
	// for the Laravel framework. 
	// 
	public static function laravel_init()
	{
		\Laravel\Autoloader::alias('\Cloudmanic\Libraries\CentCom', 'CentCom');
		\Laravel\Autoloader::alias('\Cloudmanic\Libraries\CloudAuth', 'CloudAuth');
		\Laravel\Autoloader::alias('\Cloudmanic\Libraries\AppModel', 'AppModel');
		\Laravel\Autoloader::alias('\Cloudmanic\Libraries\Me', 'Me');
		\Laravel\Autoloader::alias('\Cloudmanic\Controllers\ApiController', 'ApiController');
		\Laravel\Autoloader::alias('\Cloudmanic\Models\Users', 'Users');
		\Laravel\Autoloader::alias('\Cloudmanic\Models\OAuthSess', 'OAuthSess');
		\Laravel\Autoloader::alias('\Cloudmanic\Models\AcctUsersLu', 'AcctUsersLu');
		\Laravel\Autoloader::alias('\Cloudmanic\Models\AcctHistory', 'AcctHistory');
		\Laravel\Autoloader::alias('\Cloudmanic\Models\Accounts', 'Accounts');
	}
}

/* End File */