<?php 
//
// Company: Cloudmanic Labs, LLC
// By: Spicer Matthews 
// Email: spicer@cloudmanic.com
// Website: http://cloudmanic.com
// Date: 10/7/2012
//

namespace Cloudmanic\Libraries;

use Cloudmanic\Models\Users as Users;
use Cloudmanic\Models\AcctUsersLu as AcctUsersLu;
use Cloudmanic\Models\OAuthSess as OAuthSess;
use Laravel\Input as Input;
use Laravel\Session as Session;
use Laravel\URI as URI;
use Laravel\URL as URL;
use Laravel\Redirect as Redirect;
use Laravel\Response as Response;
use Laravel\Config as Config;

class CloudAuth
{
	public static $account = array();
	private static $error = '';
	private static $redirect = '';

	//
	// Authenticate this session. Set the "Me" object if 
	// this authentication was a success.
	//
	public static function sessioninit()
	{				
		// See if we have passed in a access_token and an account id.
		if(Input::get('access_token') && Input::get('account_id'))
		{
			$access_token = Input::get('access_token');
			$accountid = Input::get('account_id');
		} else
		{
			// See if we have a session.
			if((! Session::get('LoggedIn')) || (! Session::get('AccessToken')))
			{
				self::_cloudmanic_login();
				return false;
			} 
			
			$access_token = Session::get('AccessToken');
			$accountid = Session::get('AccountId');
		}

		// Check to see if the access token is still valid. 
		if(! $sess = self::_get_access_token_session($access_token))
		{
			self::logout();
			self::$error = 'Access token not valid.';
			return false;
		}
		
		// Make sure we have a user id in the oauth Session.
		if(! isset($sess['OauthSessUserId']))
		{
			self::logout();
			self::$error = 'Failed Authentication. (#002)';
			return false;
		}		

		// Check the database for the user and update the session.
		if(! $me = self::do_user($sess['OauthSessUserId'], $accountid, $access_token))
		{
			self::logout();
			return false;
		} else
		{
			Me::set($me);
			return $me;
		}
	} 

	//
	// We kill the current session.
	//
	public static function logout()
	{	
		Session::flush();
	}

	//
	// Setup the user. Pass in a user id. 
	// We also pass in a default account.
	//
	public static function do_user($id, $default = '', $access)
	{				
		// Get the user from the database and create a session.
		Users::set_no_account();
		Users::set_select(array('UsersId', 'UsersFirstName', 'UsersLastName', 'UsersEmail', 
		  											'UsersAvatar', 'UsersType', 'UsersStatus', 'UsersUpdatePw', 
		  											'UsersLastIn', 'UsersLastActivity', 'UsersUpdatedAt', 'UsersCreatedAt'));													
		if(! $dbuser = Users::get_by_id($id))
		{
			self::$error = 'Failed Authentication. (#003)';
		  return false;
		}	

		// Get all the accounts this user has access to.
		AcctUsersLu::set_no_account();
		AcctUsersLu::set_select(array('AccountsId', 'AccountsDisplayName', 'AccountsOwnerId',
														'ApplicationsId', 'ApplicationsName'));
		AcctUsersLu::set_order('AccountsDisplayName', 'desc');
		AcctUsersLu::set_col('AcctUsersLuUserId', $id);
		AcctUsersLu::set_col('ApplicationsCode', Config::get('cloudmanic.oauth_app_code'));
		AcctUsersLu::join_accounts();
		$dbuser['Accounts'] = AcctUsersLu::get();
		
		// We have to have at least one account.
		if(count($dbuser['Accounts']) <= 0)
		{
			self::$error = 'No accounts found. (#001)';
			return false;
		}
		
		// The default account is the first one.
		self::$account = $dbuser['Accounts'][0];
		
		// Set the default account if we passed one in.
		if(! empty($default))
		{
			$valid = false;
			foreach($dbuser['Accounts'] AS $key => $row)
			{
				if($row['AccountsId'] == $default)
				{
					self::$account = $row;
					$valid = true;
				}
			}
		}
		
		// Make sure the account id we passed in is valid for this user.
		if(! $valid)
		{
			self::$error = 'Account Id is not valid.';
			return false;
		}

		// Set the session.
		$q = array();
		$dbuser['AccessToken'] = trim($access);
		$q['UsersLastApp'] = $dbuser['UsersLastApp'] = self::$account['ApplicationsName'];
		$q['UsersLastActivity'] = $dbuser['UsersLastActivity'] = date('Y-m-d G:i:s');
		Session::put('LoggedIn', $dbuser['UsersId']);
		Session::put('AccessToken', trim($access));
		Me::set($dbuser);
		
		// Update last activity.
		Users::update($q, $dbuser['UsersId']);
		
		return $dbuser;
	}
	
	//
	// Get error message.
	//
	public static function get_error()
	{
		return self::$error;
	}
	
	//
	// Get redirect.
	//
	public static function get_redirect()
	{
		return self::$redirect;
	}
	
	//
	// Check to see if I am the owner of the account.
	//
	public static function owner_check()
	{
		return (Me::val('UsersId') == self::$account['AccountsOwnerId']);
	}
	
	// ------ Private helper private functions. --------------- //
	
	//
	// Take an access token and return the session.
	//
	private static function _get_access_token_session($access_token)
	{
		OAuthSess::set_no_account();
		OAuthSess::set_col('OauthSessToken', $access_token);
		$sess = OAuthSess::get();
		return (isset($sess[0])) ? $sess[0] : false;
	}
	
	//
	// Send the user to login via Cloudmanic.
	//
	private static function _cloudmanic_login()
	{
		// Setup the cloudmanic Provider	
		$provider = OAuth2\OAuth2::provider('cloudmanic', array(
			'id' => Config::get('cloudmanic.oauth_client_id'),
			'secret' => Config::get('cloudmanic.oauth_secret_id'),
			'scope' => Config::get('cloudmanic.oauth_scope')
		));

		// No Code? Redirect to oAuth server.
		if(! Input::get('code'))
		{
			die($provider->authorize()->send());
		} else
		{
			try
			{
				$params = $provider->access(Input::get('code'));
				
				if(! $user = $provider->get_user_info($params->access_token))
				{
					self::$error = 'Not Authorized. (#001)';
					return false;
				} else
				{							
					// Make sure we have an account set.
					if(! $acct = Session::get('AccountId'))
					{
						// Set a default accounts since we have not set it yet.
						if(isset($user['accounts'][0]['AccountsId']))
						{
							Session::put('AccountId', $user['accounts'][0]['AccountsId']);
							$acct = $user['accounts'][0]['AccountsId'];
						} else
						{
							self::$error = 'No accounts found. (#002)';
							return false;
						}
					}
					
					// Get the session.
					if(! $sess = self::_get_access_token_session($params->access_token))
					{
						self::$error = 'Not Authorized. (#003)';
						return false;
					}
					
					// Pull the user from database.
					if(! $me = self::do_user($user['credentials']['uid'], $acct, $user['credentials']['token']))
					{
						return false;
					} else
					{
						self::$redirect = URL::current();
						return $me;
					}
				}
			} catch (OAuth2_Exception $e)
			{
				self::$error = 'Not Authorized. (#004)';
				return false;
			}
		}
	}
}

/* End File */