<?php

class OAuth2_Provider_Cloudmanic extends OAuth2_Provider 
{
	public $base = 'https://accounts.cloudmanic.com/';
	public $method = 'POST';
	public $scope = '';
	
	//
	// Construct.
	//
	public function __construct(array $options = array())
	{
		// Are we in development mode?
		if(Laravel\Config::get('application.env') == 'development')
		{
			$this->base = 'http://accounts.cloudmanic.dev/';
		}
		
		parent::__construct($options);
	}
	
	//
	// Returns the authorize URL.
	//
	public function url_authorize()
	{
		return $this->base . 'oauth/authorize';
	}

	//
	// Returns the access token URL.
	//
	public function url_access_token()
	{
		return $this->base . 'oauth/access_token';
	}

	//
	// Makes an API call once we have an access token 
	// and returns profile the information on a user.
	//
	public function get_user_info($token)
	{
		$url = $this->base . 'oauth/user?'.http_build_query(array('access_token' => $token));
		
		$d = json_decode(file_get_contents($url), TRUE);
		
		if($d['status'] != 1)
		{
			return false;
		}
		
		$user = $d['data'];

		// Create a response from the request
		return array(
			'nickname' => $user['UsersFirstName'] . ' ' . $user['UsersLastName'],
			'name' => $user['UsersFirstName'] . ' ' . $user['UsersLastName'],
			'first' => $user['UsersFirstName'],
			'last' => $user['UsersLastName'],
			'email' => $user['UsersEmail'],
			'urls' => array(),
			'accounts' => $user['Accounts'],
			'credentials' => array(
				'uid' => $user['UsersId'],
				'provider' => $this->name,
				'token' => $token,
			),
		);
	}
}